﻿using System;

namespace MainProject
{
    using System.Collections;
    using System.Collections.Generic;
    using System.ComponentModel.Design;
    using System.Diagnostics;
    using DatuStructureLibrary;


    class Program
    {
        private static Random rnd = new Random();

        private static Person CreatePerson()
        {
            string firstName = "firstName" + rnd.Next(1, 100);
            string lastName = "lastName" + rnd.Next(1, 100);
            return new Person(firstName, lastName);
        }

        private static ILinearList GeneratePerson(int count)
        {
            ILinearList persons = new LinkedList();
            if (count != 0)
            {
                persons.InsertFirst(CreatePerson());
                object current = persons.AccessFirst();
                for (int i = 1; i < count; i++)
                {
                    Person person = CreatePerson();
                    persons.InsertAfter(current, person);
                    current = persons.AccessAfter(current);
                }
            }
            return persons;
        }

        /*   private static IList<Person> GeneratePerson2(long count)
           {
               IList<Person> persons = new List<Person>();
               for (long i = 0; i < count; i++)
               {
                   persons.Add(CreatePerson());
               }

               return persons;
           }*/

        /*   private static IStack<Person> GeneratePerson3(long count)
           {
               IStack<Person> persons = new Stack<Person>();
               for (long i = 0; i < count; i++)
               {
                   persons.Push(CreatePerson());
               }
               return persons;
           }*/

        static void Main(string[] args)
        {
            //int personCount = rnd.Next(0, 100);
            //ILinearList persons = GeneratePerson(200);
            //var stopwatch = new Stopwatch();
            //stopwatch.Start();
            //IList<Person> persons = GeneratePerson2(1000000);
            //stopwatch.Stop();
            // Console.WriteLine(stopwatch.Elapsed);
            //  Console.WriteLine(persons.Size);
            //  persons.Inspection(Person.PrintPerson);

            /* foreach (var person in persons)
         {
             Console.WriteLine(person);    
         }*/

            // IStack<Person> persons = GeneratePerson3(100);

            // persons.Pop();

            //  Console.WriteLine(persons);
            PriorityQueue<int> priorityQueue = new PriorityQueue<int>();
            priorityQueue.Insert(5);
            priorityQueue.Insert(50);
            priorityQueue.Insert(10);
            priorityQueue.Insert(8);
            priorityQueue.Insert(2);
            priorityQueue.Insert(15);
            priorityQueue.Insert(22);
            Console.WriteLine(priorityQueue.FindMax());
            Console.WriteLine(priorityQueue.RemoveMax());
            Console.WriteLine(priorityQueue.FindMax());
            Console.WriteLine(priorityQueue.RemoveMax());
            Console.WriteLine(priorityQueue.FindMax());
            Console.WriteLine(priorityQueue.RemoveMax());
            Console.WriteLine(priorityQueue.FindMax());
        }


    }
}
