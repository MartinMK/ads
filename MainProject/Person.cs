﻿using System;
using System.Collections.Generic;
using System.Text;

namespace MainProject
{
    using System.Reflection.Metadata.Ecma335;
    public delegate void InspectionDelegate(Person person);

    public class Person : IComparable
    {
        public String FirstName { get; set; }

        public String LastName { get; set; }

        public uint id { get; private set; }

        private static uint idCounter = 0;

        public Person(string firstName, string lastName)
        {
            this.id = idCounter++;
            this.FirstName = firstName;
            this.LastName = lastName;
        }

        public override string ToString()
        {
            return $"FirstName: {this.FirstName}, LastName: {this.LastName}, id: {this.id}";
        }

        public override int GetHashCode()
        {
            throw new NotImplementedException();
        }

        public override bool Equals(object obj)
        {
            if (this == obj) return false;
            if (!(obj is Person)) return false;
            return id == (obj as Person).id;
        }

        public static void PrintPerson(object person)
        {
            Console.WriteLine(person.ToString());
        }

        public int CompareTo(object obj)
        {
            Person second = obj as Person;
            if (LastName.CompareTo(second.LastName) != 0)
                return LastName.CompareTo(second.LastName);
            return 0;
        }
    }
}
