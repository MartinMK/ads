namespace TrainStation
{
    public class ElectricLocomotive : Locomotive
    {
        public enum TypeOfSystemFeeder { DC, AC, multiSystem, accumulator };
        private TypeOfSystemFeeder typeOfSystemFeeder;

        public ElectricLocomotive(uint locomotiveId, uint maxSpeed, uint weight, uint power, TypeOfSystemFeeder typeOfSystemFeeder) : base(locomotiveId, maxSpeed, weight, power)
        {
            this.typeOfSystemFeeder = typeOfSystemFeeder;
        }

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(typeOfSystemFeeder)}: {typeOfSystemFeeder}";
        }
    }
}