using System.Collections.Generic;

namespace TrainStation
{
    public class Station
    {
        private string name;
        public List<Platform> platforms { get; set; }

        public Station(string name, List<Platform> platforms)
        {
            this.name = name;
            this.platforms= platforms;
        }
    }
}