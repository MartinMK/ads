namespace TrainStation
{
    public class Locomotive
    {
        public uint locomotiveID { get; private set; }
        private uint maxSpeed;
        private uint weight;
        private uint power;

        public Locomotive(uint locomotiveId, uint maxSpeed, uint weight, uint power)
        {
            locomotiveID = locomotiveId;
            this.maxSpeed = maxSpeed;
            this.weight = weight;
            this.power = power;
        }

        public override string ToString()
        {
            
            return $"{nameof(maxSpeed)}: {maxSpeed}, {nameof(weight)}: {weight}, {nameof(power)}: {power}, {nameof(locomotiveID)}: {locomotiveID}";
        }
    }
}