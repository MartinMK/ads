namespace TrainStation
{
    public class Wagon
    {
        private uint dateOfProduction;
        public uint wagonId { get; private set; }

        public Wagon(uint dateOfProduction, uint wagonId)
        {
            this.dateOfProduction = dateOfProduction;
            this.wagonId = wagonId;
        }

        public override string ToString()
        {
            return $"{nameof(dateOfProduction)}: {dateOfProduction}, {nameof(wagonId)}: {wagonId}";
        }
    }
}