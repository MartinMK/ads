namespace TrainStation
{
    public class SteamLocomotive : Locomotive
    {
        private uint waterTankCapacity;

        public SteamLocomotive(uint locomotiveId, uint maxSpeed, uint weight, uint power, uint waterTankCapacity) : base(locomotiveId, maxSpeed, weight, power)
        {
            this.waterTankCapacity = waterTankCapacity;
        }

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(waterTankCapacity)}: {waterTankCapacity}";
        }
    }
}