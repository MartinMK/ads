namespace TrainStation
{
    public class CargoWagon : Wagon
    {
        private uint loadCapacity;

        public CargoWagon(uint dateOfProduction, uint wagonId, uint loadCapacity) : base(dateOfProduction, wagonId)
        {
            this.loadCapacity = loadCapacity;
        }

        public override string ToString()
        {
            return $"{nameof(loadCapacity)}: {loadCapacity}";
        }
    }
}