using System;
using System.Collections.Generic;
using System.Reflection.Metadata.Ecma335;

namespace TrainStation
{ //nastupiste 3x
    public class Platform
    {
        public uint platformNumber { get; private set; }
        private bool isEmpty;
        private Train trainInStation;
        Queue<Train> trains = new Queue<Train>();

        public Platform(uint platformNumber)
        {
            isEmpty = true;
            this.platformNumber = platformNumber;
        }


        bool addTraion(Platform platform, Train train)
        {
            if (platform.isEmpty)
            {
                platform.trainInStation = train;
                platform.isEmpty = false;
                return true;
            }

            return false;
        }

        Train removeTrain(Platform platform)
        {
            if (platform.isEmpty)
                return null;
            Train removedTrain = platform.trainInStation;
            trainInStation = null;
            platform.isEmpty = true;
            return removedTrain;
        }

        public void trainArrive(Train train)
        {
            trains.Enqueue(train);
        }

        public Train trainLeave()
        {
            if (trains.Count == 0)
                throw new Exception();
            return trains.Dequeue();
        }

        public void moveToPlatform()
        {
            Train train = trainLeave();
            removeTrain(this);
            trainArrive(train);
            addTraion(this, train);
        }
    }
}