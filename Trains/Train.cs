using System;
using System.Collections.Generic;

namespace TrainStation
{
    public class Train
    {
        public uint TrainId { get; private set; }
        private Stack<Locomotive> Locomotives;
        private List<Wagon> Wagons;
        private string name;

        public Train(string name, uint trainId)
        {
            this.name = name;
            TrainId = trainId;
            Locomotives = new Stack<Locomotive>();
            Wagons = new List<Wagon>();
        }

        public void addLocomotive(Train train, Locomotive newLocomotive)
        {
            train.Locomotives.Push(newLocomotive);
        }

        public Locomotive removeFirstLocomotive(Train train)
        {
            return train.Locomotives.Count > 1 ? train.Locomotives.Pop() : null;
        }

        public void addWagon(Train train, Wagon wagon)
        {
            train.Wagons.Add(wagon);
        }

        public Wagon removeLastWagon(Train train)
        {
            if (train.Wagons.Count == 0)
                throw new ArgumentOutOfRangeException();
            Wagon returnedWagon = train.Wagons[train.Wagons.Count - 1];
            train.Wagons.RemoveAt(train.Wagons.Count - 1);
            return returnedWagon;
        }

        public uint lenghtOfTrain(Train train)
        {
            return (uint) (train.Locomotives.Count + train.Wagons.Count);
        }

        public override string ToString()
        {
            String s = $"{nameof(name)}: {name}, {nameof(TrainId)}: {TrainId}";
            foreach (var locomotive in Locomotives)
            {
                s += locomotive.ToString();
            }
            for (int i = 0; i < Wagons.Count; i++)
            {
                s+=Wagons[i].ToString();
                
            }
            return s;
        }
    }
}