﻿using System;
using System.Collections;
using System.Collections.Generic;

namespace TrainStation
{
    class Program
    {
        static void Main(string[] args)
        {
            Station hradecKralove = prepareStation("Hradec Kralove");
            List<Train> trains = prepareTrains(100);
            for (int i = 0; i < trains.Count; i++)
            {
                var s = trains[i];
                Console.WriteLine(s);
            }
            
            hradecKralove.platforms[0].trainArrive(trains[0]);
            hradecKralove.platforms[0].moveToPlatform();
            hradecKralove.platforms[0].trainLeave();

        }

        static List<Locomotive> prepareLocomotive()
        {
            List<Locomotive> locomotives = new List<Locomotive>();
            locomotives.Add(new SteamLocomotive(1, 80, 99000, 1500, 15));
            locomotives.Add(new SteamLocomotive(2, 90, 90000, 1300, 15));
            locomotives.Add(new SteamLocomotive(3, 50, 69000, 552, 10));
            locomotives.Add(new DieselLocomotive(4, 40, 22000, 147, 8500));
            locomotives.Add(new DieselLocomotive(5, 80, 69500, 600, 18500));
            locomotives.Add(new ElectricLocomotive(6, 110, 72000, 880, ElectricLocomotive.TypeOfSystemFeeder.DC));
            locomotives.Add(new ElectricLocomotive(7, 140, 85000, 6400, ElectricLocomotive.TypeOfSystemFeeder.AC));
            return locomotives;
        }

        static List<Wagon> prepareWagons()
        {
            List<Wagon> wagons = new List<Wagon>();
            wagons.Add(new PersonalWagon(1985, 1, 40));
            wagons.Add(new PersonalWagon(1995, 2, 35));
            wagons.Add(new PersonalWagon(1968, 3, 50));
            wagons.Add(new DiningCar(2003, 4));
            wagons.Add(new CargoWagon(1995, 5, 80000));
            return wagons;
        }

        static List<Train> prepareTrains(uint count)
        {
            Random rnd = new Random();
            List<Locomotive> locomotives = prepareLocomotive();
            List<Wagon> wagons = prepareWagons();
            List<Train> trains = new List<Train>();
            Train newTrain;
            for (int i = 0; i < count; i++)
            {
                newTrain = new Train("Vlak" + i, (uint) (i + 1));
                for (int j = 0; j < rnd.Next(1, locomotives.Count); j++)
                {
                    newTrain.addLocomotive(newTrain, locomotives[j]);
                }

                for (int j = 0; j < rnd.Next(0, wagons.Count); j++)
                {
                    newTrain.addWagon(newTrain, wagons[j]);
                }

                trains.Add(newTrain);
            }

            return trains;
        }

        static Station prepareStation(string name)
        {
            Platform platform1 = new Platform(1);
            Platform platform2 = new Platform(2);
            Platform platform3 = new Platform(3);
            List<Platform> platforms = new List<Platform>();
            platforms.Add(platform1);
            platforms.Add(platform2);
            platforms.Add(platform3);
            return new Station(name, platforms);
        }
    }
}