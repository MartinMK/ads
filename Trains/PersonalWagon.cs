namespace TrainStation
{
    public class PersonalWagon : Wagon
    {
        private uint capacity;

        public PersonalWagon(uint dateOfProduction, uint wagonId, uint capacity) : base(dateOfProduction, wagonId)
        {
            this.capacity = capacity;
        }

        public override string ToString()
        {
            return $"{nameof(capacity)}: {capacity}";
        }
    }
}