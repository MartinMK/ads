namespace TrainStation
{
    public class DieselLocomotive : Locomotive
    {
        private uint fuelTankCapacity;

        public DieselLocomotive(uint locomotiveId, uint maxSpeed, uint weight, uint power, uint fuelTankCapacity) : base(locomotiveId, maxSpeed, weight, power)
        {
            this.fuelTankCapacity = fuelTankCapacity;
        }

        public override string ToString()
        {
            return $"{base.ToString()}, {nameof(fuelTankCapacity)}: {fuelTankCapacity}";
        }
    }
}