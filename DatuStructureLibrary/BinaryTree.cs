﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatuStructureLibrary
{
    public class BinaryTree<T> : IComparable<BinaryTree<T>> where T : IComparable<T>
    {
        private IList<Node> tree = new List<Node>();

        public class Node
        {
            public Node(int index, T data)
            {
                this.index = index;
                this.data = data;
            }

            public int index;
            public T data;
        }

  public void Insert(T data)
        {
            if (this.IsEmpty())
            {
                InsertRoot(data);
                return;
            }
            Node currentNode = this.tree[0];
            if (currentNode.data.CompareTo(data) < 0)
                InsertToLeft(data, currentNode.index + 1);
            InsertToRight(data, currentNode.index + 2);

        }

        private void InsertToRight(T data, int index)
        {
            if (this.tree[index] == null)
                this.tree.Insert(index, new Node(index, data));
            if (tree[index].data.CompareTo(data) < 0)
                InsertToLeft(data, index + 1);
            InsertToRight(data, index + 2);
        }

        private void InsertToLeft(T data, int index)
        {
            if (this.tree[index] == null)
                this.tree.Insert(index, new Node(index, data));
            if (tree[index].data.CompareTo(data) < 0)
                InsertToLeft(data, index + 1);
            InsertToRight(data, index + 2);
        }

        public T Delete(T data)
        {
            throw new NotImplementedException();
        }

        public T Search(T data)
        {
            Node currentNode = this.tree[0];
            if (currentNode.data.CompareTo(data) == 0)
                return currentNode.data;
            if (currentNode.data.CompareTo(data) < 0)
                return AccessLeft(data, currentNode.index + 1);
            return AccessRight(data, currentNode.index + 2);
        }

        private T AccessRight(T data, int index)
        {
            Node node = null;
            if (this.tree[index] == null)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (this.tree[index].data.CompareTo(data) == 0)
                node = this.tree[index];
            if (data.CompareTo(this.tree[index].data) < 0)
                AccessLeft(data, index + 1);
            AccessRight(data, index + 2);

            return node.data;
        }

        private T AccessLeft(T data, int index)
        {
            Node node = null;
            if (this.tree[index] == null)
            {
                throw new ArgumentOutOfRangeException();
            }
            if (this.tree[index].data.CompareTo(data) == 0)
                node = this.tree[index];
            if (data.CompareTo(this.tree[index].data) < 0)
                AccessLeft(data, index + 1);
            AccessRight(data, index + 2);

            return node.data;
        }

        private void InsertRoot(T data)
        {
            this.tree.Add(new Node(0, data));
        }

        bool IsEmpty()
        {
            return this.tree.Count == 0 ? true : false;
        }

        void Clear()
        {
            this.tree.Clear();
        }

        private int CompareTo(BinaryTree<T> other)
        {
            throw new NotImplementedException();
        }

        public void Traverse(Node node)
        {
            if (node == null)
            {
                return;
            }
            Traverse(this.tree[node.index + 1]);
            Traverse(this.tree[node.index + 1]);
        }

        int IComparable<BinaryTree<T>>.CompareTo(BinaryTree<T> other)
        {
            if (other == null)
                return 1;
            return this.CompareTo(other);
        }
    }
}
