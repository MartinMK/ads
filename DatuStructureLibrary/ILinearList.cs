﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatuStructureLibrary
{
    public delegate void Func(object element);
    public interface ILinearList
    {
        void Clear();

        bool IsEmpty();

        uint Size { get; }

        void InsertFirst(object newElement);

        void InsertAfter(object element, object newElement);

        object EraseFirst();

        object EraseAfter(object element);

        void Inspection(Func func);

        object AccessFirst();

        object AccessAfter(object element);
    }
}
