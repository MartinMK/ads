﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;

namespace DatuStructureLibrary
{
    public class Stack<T> : IStack<T>, IEnumerable where T : IComparable
    {
        private IList<T> innerStructure = new List<T>();

        public Stack()
        {
        }

        public int Count
        {
            get
            {
                return this.innerStructure.Count;
            }
        }

        public bool Empty
        {
            get
            {
                return this.innerStructure.Count == 0;
            }
        }

         IEnumerator<T> GetEnumerator()
        {
            return this.innerStructure.GetEnumerator();
        }

         IEnumerator IEnumerable.GetEnumerator(){
            return GetEnumerator();
        }

        public T Peek()
        {
            if (this.Empty)
                throw new NullReferenceException();
            return this.innerStructure[this.innerStructure.Count - 1];
        }

        public T Pop()
        {

            T removedObject = this.Peek();
            this.innerStructure.RemoveAt(this.innerStructure.Count - 1);
            return removedObject;
        }

        public void Push(T newElement)
        {
            innerStructure.Add(newElement);
        }

        public void Clear()
        {
            this.innerStructure.Clear();
        }



    }
}
