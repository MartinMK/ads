﻿namespace DatuStructureLibrary
{
    using System;
    using System.Reflection.Metadata;

    //    ctrl + shift + b = build
    //    ctr + f5 = start
    //    alt + ft10 = content menu
    //    alt + enter = quick actions
    //    f5 = debbug
    //    ctr + shift + a = new item
    //ctr + k , ctr + d = format code
    //crk + k / c
    //tab tab - generate code
    //cw
    //prop - get/set
    //propg


    public class LinkedList : ILinearList
    {
        protected class Node
        {
            public Node After { get; set; }

            public object Data { get; set; }

        }

        private Node first = null;
        private uint size;

        public uint Size
        {
            get
            {
                return size;
            }
        }

        public LinkedList()
        {
            size = 0;
        }

        public object AccessAfter(object element)
        {
            Node current = findNode(element);
            if (current.After == null)
                throw new NullReferenceException();
            return current.After.Data;
        }

        public object AccessFirst()
        {
            if (this.IsEmpty())
                throw new NullReferenceException();
            return this.first.Data;
        }


        public void Clear()
        {
            this.first = null;
            this.size = 0;
        }

        public object EraseAfter(object element)
        {
            if (this.IsEmpty()) throw new NullReferenceException();
            Node currentNode = findNode(element);
            if (currentNode == null) throw new NullReferenceException();
            object eraseNodeData = currentNode.After.Data;
            currentNode.After = currentNode.After.After;
            this.size--;
            return eraseNodeData;
        }

        public object EraseFirst()
        {
            if (this.IsEmpty())
                throw new NullReferenceException();
            Node firstNode = this.first;
            this.first = firstNode.After;
            this.size--;
            return firstNode.Data;

        }

        public void InsertFirst(object newElement)
        {
            Node node = new Node
            {
                After = this.first,
                Data = newElement
            };
            first = node;
            this.size++;
        }

        public void InsertAfter(object element, object newElement)
        {
            Node current = findNode(element);

            Node node = new Node
            {
                After = current.After,
                Data = newElement
            };
            current.After = node;
            this.size++;
        }

        private Node findNode(object element)
        {
            Node current = this.first;
            while (current == null)
            {
                if (current.Data == element)
                    break;
                current = current.After;
            }

            if (current == null)
            {
                throw new Exception();
            }

            return current;
        }


        public bool IsEmpty()
        {
            return this.size == 0;
        }

        public void Inspection(Func func)
        {
            Node current = this.first;
            while (current != null)
            {
                func(current.Data);
                current = current.After;
            }
        }
    }
}