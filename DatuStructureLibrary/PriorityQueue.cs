using System;
using System.ComponentModel.Design.Serialization;

namespace DatuStructureLibrary
{
    public class PriorityQueue<T> where T : IComparable<T>
    {
        BinarySearchTree<T> prio = new BinarySearchTree<T>();

       public void Insert(T item)
        {
            prio.InsertNode(item);
        }

    public T FindMax()
        {
            return prio.FindMax();
        }

      public T RemoveMax()
       {
           return prio.RemoveNode(prio.FindMax());
       }
    }
}