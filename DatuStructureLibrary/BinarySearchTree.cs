using System;

namespace DatuStructureLibrary
{
    public class BinarySearchTree<T> : IComparable<BinarySearchTree<T>> where T : IComparable<T>

    {
        public class Node : IComparable<Node>
        {
            public T data;
            public Node left;
            public Node right;
            public Node parent;

            public Node(T data, Node left, Node right, Node parent)
            {
                this.data = data;
                this.left = left;
                this.right = right;
                this.parent = parent;
            }

            public int CompareTo(Node other)
            {
                if (other == null)
                    return 1;
                return this.data.CompareTo(other.data);
            }
        }

        public int CompareTo(BinarySearchTree<T> other)
        {
            if (other == null)
                return 1;
            return this.CompareTo(other);
        }

        private Node root;

        public BinarySearchTree()
        {
            root = null;
        }

        public void InsertNode(T data)
        {
            Insert(root, data);
        }

        public T FindNode(T data)
        {
            return Find(data, root);
        }

        public void Insert(Node start, T data)
        {
            if (IsEmpty())
            {
                root = new Node(data, null, null, null);
                return;
            }

            if (data.CompareTo(start.data) == 1)
            {
                if (start.right == null)
                    start.right = new Node(data, null, null, start);
                Insert(start.right, data);
            }

            if (data.CompareTo(start.data) == -1)
            {
                if (start.left == null)
                    start.left = new Node(data, null, null, start);
                Insert(start.left, data);
            }
        }

        public T Find(T data, Node start)
        {
            if (IsEmpty())
                throw new NullReferenceException();
            if (start.data.CompareTo(data) == 0)
                return start.data;
            if (start.data.CompareTo(data) == -1)
                return Find(data, start.right);
            if (start.data.CompareTo(data) == 1)
                return Find(data, start.left);
            return default(T);
        }

        public Node FindNode(T data, Node start)
        {
            if (IsEmpty())
                throw new NullReferenceException();
            if (start.data.CompareTo(data) == 0)
                return start;
            if (start.data.CompareTo(data) == -1)
                return FindNode(data, start.right);
            if (start.data.CompareTo(data) == 1)
                return FindNode(data, start.left);
            return null;
        }

        public Node FindMaxNode(Node start)
        {
            if (IsEmpty())
                throw new NullReferenceException();
            if (start.right == null)
                return start;
            return this.FindMaxNode(start);
        }


        public T FindMax()
        {
            return FindMax(root);
        }



        public T FindMax(Node start)
        {
            if (IsEmpty())
                throw new NullReferenceException();
            if (start.right == null)
                return start.data;
            return FindMax(start.right);
        }

        public T RemoveNode(T data)
        {
            return Remove(this.root, data).data;
        }

        public Node Remove(Node start, T data)
        {
            if (IsEmpty())
                throw new NullReferenceException();
            if (start.data.CompareTo(data) == 1)
                start.left = Remove(start.left, data);
            else if (start.data.CompareTo(data) == -1)
                start.right = Remove(start.right, data);
            else
            {
                if (start.left == null)
                    return start.right;
                else if (start.right == null)
                    return start.left;
                start.data = MinValue(start.right);
                start.right = Remove(start.right, start.data);
            }
            return start;
        }

        private T MinValue(Node start)
        {
            T minValue = start.data;
            while (start.left != null)
            {
                minValue = start.left.data;
                start = start.left;
            }

            return minValue;
        }

        public void Traverse(Node start)
        {
            if (IsEmpty())
            {
                return;
            }

            Traverse(start.left);
            Traverse(start.right);
        }

        public bool IsEmpty()
        {
            if (root == null)
                return true;
            return false;
        }

        public void Clear()
        {
            root = null;
        }
    }
}