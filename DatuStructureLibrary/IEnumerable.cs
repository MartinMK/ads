﻿namespace DatuStructureLibrary
{
    public interface IEnumerable<T> where T : IEnumerable<T>
    {
    }
}