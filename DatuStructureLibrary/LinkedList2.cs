﻿using System;
using System.Collections.Generic;
using System.Text;

namespace DatuStructureLibrary
{
   public class LinkedList2 : ILinearList
    {
        protected class Node
        {
            public Node After { get; set; }

            public object Data { get; set; }

        }

        private Node currentNode = null;

        private Node first = null;
        private uint size;

        public uint Size
        {
            get
            {
                return size;
            }
        }

        public LinkedList2()
        {
            size = 0;
        }

        public object AccessAfter(object element)
        {
            Node afterNode = this.currentNode.After;
            if (afterNode == null)
                return null;
            return afterNode;
        }

        public object AccessFirst()
        {
            if (this.IsEmpty())
                throw new NullReferenceException();
            return this.first.Data;
        }

        public void Clear()
        {
            this.first = null;
            this.size = 0;
            this.currentNode = null;
        }

        public object EraseAfter(object element)
        {
            Node current = FindNode(element);
            if (current == null)
                return null;
            Node afterNode = this.currentNode.After;
            this.currentNode.After = afterNode.After;
            return this.currentNode.After;
        }

        public object EraseFirst()
        {
            if (this.IsEmpty())
                return null;
            Node tmp = this.first;
            Node after = tmp.After;
            this.first = after;
            return tmp;
        }

        public void InsertFirst(object newElement)
        {
            Node node = new Node
            {
                After = this.first,
                Data = newElement
            };
            first = node;
            this.size++;
        }

        public void InsertAfter(object element, object newElement)
        {
            Node current = FindNode(element);



            Node node = new Node
            {
                After = null,
                Data = newElement
            };

            node.After = current.After;
            current.After = node;

            this.size++;
        }

        public void Inspection()
        {
            Node current = this.first;
            while (current == null)
            {

            }
        }

        public bool IsEmpty()
        {
            return this.size == 0;
        }

        Node FindNode(object element)
        {
            Node current = this.first;
            while (current == null)
            {
                if (current.Data == element)
                    break;
                current = this.currentNode.After;
            }
            if (current == null)
            {
                throw new IndexOutOfRangeException("null");
            }
            return current;
        }

        public void Inspection(Func func)
        {
            throw new NotImplementedException();
        }
    }
}
