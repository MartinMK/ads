﻿namespace DatuStructureLibrary
{
    public interface IStack<T> 

    {
        int Count { get; }
        bool Empty { get; }
        T Peek();
        T Pop();
        void Push(T newElement);
        void Clear();
    }
}